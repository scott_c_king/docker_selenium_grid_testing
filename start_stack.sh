#!/bin/bash

# Start the stack
echo -e "\nINFO: Starting Selenium-Hub:"
docker run -d -p 4444:4444 --name selenium-hub selenium/hub
echo -e "\nINFO: Starting Selenium Nodes:"
docker run -d -p 32768:5900 -P --link selenium-hub:hub selenium/node-chrome-debug
docker run -d -p 32769:5900 -P --link selenium-hub:hub selenium/node-firefox-debug
echo -e "\nINFO: Stack start complete."
echo -e "Chrome Node VNC  : 0.0.0.0:32768"
echo -e "Firefox Node VNC : 0.0.0.0:32769"
