#!/bin/bash

# Stop the stack
echo -e "\nINFO: Stopping the stack..."
for i in `docker ps | grep hub | awk '{ print $1 }'` ; do docker stop $i ; docker rm $i ; done
for i in `docker ps | grep node | awk '{ print $1 }'` ; do docker stop $i ; docker rm $i ; done
echo -e "INFO: Stack shut-down complete."