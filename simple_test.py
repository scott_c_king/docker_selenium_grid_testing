#!/usr/bin/env python3

'''
Simple Selenium Test
'''

__author__ = "Scpott C. King"
__version__ = "0.1.0"
__license__ = "MIT"

import time
import unittest
from selenium import webdriver


class DragosTest(unittest.TestCase):
    ''' I do very little. '''

    # Local operation
    #def setUp(self):
    #    self.driver = webdriver.Firefox()

    # Grid/Hub operation (remote webdriver)
    def setUp(self):
        self.driver = webdriver.Remote(
            command_executor='http://127.0.0.1:4444/wd/hub',
            desired_capabilities={'browserName': 'firefox'})

    def test_title(self):
        ''' Wimpy. '''
        print('INFO: Testing Web Page Title')
        driver = self.driver
        driver.get('https://dragos.com/')
        print('Driver Title:', driver.title)
        assert 'Dragos' in driver.title
        time.sleep(5)

    def tearDown(self):
        ''' Clean up the mess. '''
        self.driver.quit()


if __name__ == "__main__":
    unittest.main(warnings='ignore')
