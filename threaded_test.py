#!/usr/bin/env python3

'''
Multi-threaded Selenium Test
'''

__author__ = "Scott C. King"
__version__ = "0.1.0"
__license__ = "MIT"

import time
from threading import Thread
from selenium import webdriver


CMD_EXECUTOR = 'http://127.0.0.1:4444/wd/hub'
BROWSERS = [{"browserName": "firefox"},
            {"browserName": "chrome"}]
WAITING = []


def start_browser(capabilities):
    ''' Start browsers. '''
    return webdriver.Remote(desired_capabilities=capabilities,
                            command_executor=CMD_EXECUTOR)


def get_browser_and_wait(browser_data):
    ''' Start browser and wait. '''
    print('INFO: Starting {}'.format(browser_data['browserName'].title()))
    browser = start_browser(browser_data)
    browser.get('http://dragos.com')
    WAITING.append({'data': browser_data, 'driver': browser})


def main():
    ''' Do the work. '''
    threads = []
    for browser in BROWSERS:
        thread = Thread(target=get_browser_and_wait, args=[browser])
        threads.append(thread)
        thread.start()

    for thread in threads:
        thread.join()

    print('INFO: Browsers are ready...')
    time.sleep(5)

    for browser in WAITING:
        print('INFO: Cleaning up {}'.format(browser['data']['browserName'].title()))
        browser['driver'].quit()


if __name__ == "__main__":
    main()
